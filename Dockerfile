# ubuntu-nodejs

ARG DOCKER_REGISTRY_URL=registry.gitlab.com/docking/
ARG CUSTOM_VERSION=1.0
ARG UBUNTU_VERSION=18.04
ARG NODEJS_VERSION=14.15.5

ARG KCOV_VERSION=38


FROM ${DOCKER_REGISTRY_URL}ubuntu-nodejs-mini:${CUSTOM_VERSION}-${UBUNTU_VERSION}-${NODEJS_VERSION} AS ubuntu-nodejs

ARG KCOV_VERSION

WORKDIR /tmp

RUN \
	apt-get -q -y update ; \
	apt-get -q -y install \
		binutils-dev \
		build-essential \
		clang \
		cmake \
		curl \
		debsigs \
		jq \
		git \
		libclang-dev \
		libdw-dev \
		libiberty-dev \
		libjemalloc-dev \
		libpcap-dev \
		librocksdb-dev \
		libssl-dev \
		llvm \
		make \
		node-gyp \
		openssl \
		pkg-config \
		python-minimal \
		python3-minimal \
		unzip \
		zip \
		zlib1g-dev \
	; \
	apt-get -q -y clean ; \
	find /var/lib/apt/lists/ -type f -delete

RUN \
	curl -Lo kcov-${KCOV_VERSION}.tar.gz https://github.com/SimonKagstrom/kcov/archive/v${KCOV_VERSION}.tar.gz && ( \
		tar zxf kcov-${KCOV_VERSION}.tar.gz ; \
		mkdir -p kcov-${KCOV_VERSION}/build ; \
		cd kcov-${KCOV_VERSION}/build ; \
		cmake .. ; \
		make -j $(nproc) ; \
		make install ; \
	) ; \
	rm -fR kcov-${KCOV_VERSION} kcov-${KCOV_VERSION}.tar.gz
